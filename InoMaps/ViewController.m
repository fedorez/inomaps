//
//  ViewController.m
//  InoMaps
//
//  Created by Vladislav Grigoriev on 12/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "INOUserAnnotation.h"

static NSString *const ViewControllerMapViewPin = @"ViewControllerMapViewPin";

@interface ViewController () <CLLocationManagerDelegate, MKMapViewDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) MKMapView *mapView;

@property (nonatomic, strong) INOUserAnnotation *userAnnotation;

@end

@implementation ViewController

- (void)loadView {
    self.mapView = [[MKMapView alloc] init];
    self.view = self.mapView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userAnnotation = [[INOUserAnnotation alloc] init];

    [self.mapView addAnnotation:self.userAnnotation];
    self.mapView.delegate = self;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.allowsBackgroundLocationUpdates = YES;
    
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
   
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
        [self.locationManager startUpdatingHeading];
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
//    [pointAnnotation setCoordinate:[self.mapView convertPoint:[touches.anyObject locationInView:self.mapView] toCoordinateFromView:self.mapView]];
//    [self.mapView addAnnotation:pointAnnotation];
//}

#pragma mark - MKMapViewDelegate

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:ViewControllerMapViewPin];
    if (!pinView) {
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                  reuseIdentifier:ViewControllerMapViewPin];
    }
    
    pinView.leftCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeContactAdd];
    pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    pinView.image = [UIImage imageNamed:@"account-circle"];
    pinView.canShowCallout = YES;
    return pinView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if(status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
        [self.locationManager startUpdatingHeading];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    NSLog(@"%@", locations);
    [self.userAnnotation setCoordinate:[locations.lastObject coordinate]];
    [self.mapView.camera setCenterCoordinate:[locations.lastObject coordinate]];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    NSLog(@"%@", newHeading);
    [self.mapView.camera setHeading:newHeading.trueHeading];
    
    if (ABS(newHeading.trueHeading - 360.0f) < 10.0f) {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = @"North";
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    }

}


@end
