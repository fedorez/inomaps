//
//  INOUserAnnotation.h
//  InoMaps
//
//  Created by Vladislav Grigoriev on 12/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface INOUserAnnotation : NSObject <MKAnnotation>

@end
